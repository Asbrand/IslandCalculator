/*
 * Author: kkolchanov 28.08.2018
 * Copyright none
 */
package CountIslands;

import CountIslands.DAO.IMapDao;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Обработчик изображений островов
 *
 * @version 1.0 28 Aug 2018
 * @author kkolchanov
 */
public class ImageController {

    /**
     * Переводит изображение в карту-массив
     * @param connector источник данных куда будет загружена карта
     * @param imgFile файл изображения
     * @return
     */
    public boolean[][] parseImage(IMapDao connector, File imgFile) {
        BufferedImage image = null;
        int w;//ширина картинки
        int h;//высота картинки
        boolean[][] map;

        try {
            image = ImageIO.read(imgFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        w = image.getWidth();
        h = image.getHeight();
        System.out.println("width, height: " + w + ", " + h);

        map = new boolean[w][h];
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                int pixel = image.getRGB(j, i);
                if (pixel == -1)
                    map[j][i] = false;
                else
                    map[j][i] = true;
            }
        }

        return map;
    }
}
