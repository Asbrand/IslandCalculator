/*
 * Author: kkolchanov 28.08.2018
 * Copyright none
 */
package CountIslands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import CountIslands.DAO.IMapDao;
import CountIslands.DAO.SpringJdbc;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DataSourceConfig.class, loader=AnnotationConfigContextLoader.class)
public class AppTest {

    @Autowired IMapDao dao;
    @Autowired private IslandCalculator calculator;

    @Test
    public void DataConnectionGetDataText() {
        String map= dao.getMapRaw(1);
        assertTrue(map.startsWith("7 8\\r\\n*~~~~~~~"));
    }

    @Test
    public void DataConnectorParseMap() {
        dao.getMap(1);
    }

    @Test
    public void basicPostgre() {
        int islandsCount = calculator.countIslands( 1);
        assertEquals("Количество островов должно быть равно 4", 4, islandsCount);
    }

    @Test(expected = IllegalArgumentException.class)
    public void noItemPostgre() {
        int islandsCount = calculator.countIslands( 2);
    }

    @Test
    public void bigMapPostgre() {
        int islandsCount = calculator.countIslands( 4);
        assertEquals("Количество островов должно быть равно 10", 10, islandsCount);
    }


}
