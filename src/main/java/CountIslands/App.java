/*
 * Author: kkolchanov 24.08.2018
 * Copyright none
 */
package CountIslands;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


/**
 * Инициализирует коннектор и считает острова
 *
 * @version 1.0 24 Aug 2018
 * @author kkolchanov
 */

@SpringBootApplication
public class App implements CommandLineRunner {

    @Autowired private IslandCalculator calculator;

    public static void main(String... args) {
        SpringApplication app = new SpringApplication(DataSourceConfig.class);
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        try {
            int islandsCount = calculator.countIslands(1);
            System.out.println(islandsCount);
        }catch (Exception e)
        {
            System.out.println(e);
        }
    }
}
