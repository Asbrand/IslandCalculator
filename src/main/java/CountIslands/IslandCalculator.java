/*
 * Copyright none
 */
package CountIslands;

import CountIslands.DAO.IMapDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Считает количество островов на карте
 *
 * @version 1.1 28 Aug 2018
 * @author kkolchanov
 */
@Component
public class IslandCalculator {
    boolean map[][];

    @Autowired
    IMapDao connector;

    /**
     * Считаем количество островов
     * Если натыкаемся на сушу, то убираем всю землю, что прилегает к первой найденной клетке.
     * Количество столкновений с сушей будет равно количеству островов
     *
     * @return количество островов
     */
    public int countIslands(int id) {
        map = connector.getMap(id);
        if(map==null) throw new IllegalArgumentException("Не удалось получить карту");
        int islandsCount = 0;

        for (int y = 0; y < map[0].length; y++)
            for (int x = 0; x < map.length; x++) {
                if (map[x][y]) {
                    islandsCount++;
                    floodIsland(x, y);
                }
            }
        return islandsCount;
    }

    /**
     * "Топим" остров, что нашли, дабы он не мешался на карте при дальнейшей работе
     *
     * @param x coordinate
     * @param y coordinate
     */
    private void floodIsland(int x, int y) {
        ArrayList<Coordinate> coord = new ArrayList<>();
        coord.add(new Coordinate(x, y));

        while (coord.size() > 0) {
            Coordinate firstInLine = coord.get(0);
            /* Если еще земля, то обрабатываем */
            if (map[firstInLine.x][firstInLine.y]) {
                map[firstInLine.x][firstInLine.y] = false;

                /* Проверяем соседей */
                /* нижняя клетка */
                if (firstInLine.x - 1 >= 0 && map[firstInLine.x - 1][firstInLine.y]) {
                    coord.add(new Coordinate(firstInLine.x - 1, firstInLine.y));
                }
                /* верхняя клетка */
                if (firstInLine.x + 1 < map.length && map[firstInLine.x + 1][firstInLine.y]) {
                    coord.add(new Coordinate(firstInLine.x + 1, firstInLine.y));
                }
                /* правая клетка */
                if (firstInLine.y + 1 < map[0].length && map[firstInLine.x][firstInLine.y + 1]) {
                    coord.add(new Coordinate(firstInLine.x, firstInLine.y + 1));
                }
                /* левая клетка */
                if (firstInLine.y - 1 >= 0 && map[firstInLine.x][firstInLine.y - 1]) {
                    coord.add(new Coordinate(firstInLine.x, firstInLine.y - 1));
                }
            }
            coord.remove(0);
        }

    }

    /**
     * Структура для сохранения пары координат x y
     */
    private class Coordinate {
        public int x;
        public int y;

        public Coordinate(int _x, int _y) {
            this.x = _x;
            this.y = _y;
        }
    }
}
