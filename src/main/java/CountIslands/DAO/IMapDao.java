/*
 * Copyright none
 */
package CountIslands.DAO;

/**
 * Интерфейс для работы с источниками данных
 */
public interface IMapDao {
    public String getMapRaw(long id);
    public boolean[][] getMap(long id);
}
