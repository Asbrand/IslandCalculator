/*
 * Copyright none
 */
package CountIslands.DAO;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Getting data through the hibernate
 */
public class Hibernate extends DataConnector {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public String getMapRaw(long id) {
        Session session = sessionFactory.getCurrentSession();

        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("from MapEntity where id= :id");
        query.setLong("id", id);
        MapEntity map = (MapEntity) query.uniqueResult();

        return map.getMap();
    }

}
