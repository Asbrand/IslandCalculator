package CountIslands.DAO;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by kkolchanov on 05.09.2018.
 */
public class MapEntityRowMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        MapEntity map = new MapEntity();
        map.setId(resultSet.getInt(1));
        map.setMap(resultSet.getString(2));
        return map;
    }
}
