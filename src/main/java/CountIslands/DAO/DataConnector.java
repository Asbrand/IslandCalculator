/*
 * Copyright none
 */
package CountIslands.DAO;

import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

/**
 * Реализует методы для разбора строки карты на удобный массив и наследует интерфейс
 */
public abstract class DataConnector implements IMapDao {

    @Value("${mapConfig}")
    private String mapPropertiesFile;
    private static int sMinSize = 0;
    private static int sMaxSize = 0;
    private static char sWaterChar = 'X';
    private static char sLandChar = 'X';

    @Override
    public boolean[][] getMap(long id) {
        return ParseStringToMap(getMapRaw(id));
    }

    public boolean[][] ParseStringToMap(String mapRaw) {
        String mapString = mapRaw;
        ArrayList<String> lines = new ArrayList<>(Arrays.asList(mapString.split("\\\\r\\\\n"))); //разбитая по строкам карта

        String arrayInfo = lines.get(0).trim(); //строка с определением массива
        String[] arrayInfoParts = arrayInfo.split(" "); //разбиваем строку на данные о x и y
        boolean[][] map; //карта островов
        int N; //высота массива(Y)
        int M; //ширина массива(X)

        /* Получаем параметры карты */
        getSettings();

        if (arrayInfoParts.length < 2)
            throw new IllegalArgumentException("Некорректные параметры карты: недостаточно данных");
        lines.remove(0); //убираем параметры из карты

        N = Integer.parseInt(arrayInfoParts[0]);
        M = Integer.parseInt(arrayInfoParts[1]);

        if (sMinSize > N || N > sMaxSize || sMinSize > M || M > sMaxSize) {
            throw new IllegalArgumentException(String.format("Некорректные параметры карты: Высота или ширина не попадают в границы %1$d<= N,M <=%2$d", sMinSize, sMaxSize));
        }

        /* Переводим в удобный массив */
        map = new boolean[M][N];

        if (lines.size() != N) throw new IllegalArgumentException("Некорректное количество строк");
        for (int y = 0; y < lines.size(); y++) {
            String line = lines.get(y);
            char[] elements = line.toCharArray();

            if (elements.length != M) {
                throw new IllegalArgumentException("Некорректное число клеток в строке " + (y + 1));
            }

            for (int x = 0; x < M; x++) {
                if (elements[x] == sWaterChar) {
                    map[x][y] = false;
                } else if (elements[x] == sLandChar) {
                    map[x][y] = true;
                } else {
                    throw new IllegalArgumentException(String.format("Неизвестный символ '%c' в строке %d", elements[x], y + 1));
                }
            }
        }
        return map;
    }

    /**
     * Загружаем настройки из файла
     */
    private void getSettings() {
        java.util.Properties prop = new Properties();

        InputStream propertiesInputStream = getClass().getClassLoader().getResourceAsStream(mapPropertiesFile);
        try {
            prop.load(propertiesInputStream);
        } catch (IOException e) {
            System.out.println("Не удалось загрузить настройки. Оставляем стандартные");
            e.printStackTrace();
        }

        sMinSize = Integer.parseInt(prop.getProperty("sMinSize"));
        sMaxSize = Integer.parseInt(prop.getProperty("sMaxSize"));
        sWaterChar = prop.getProperty("sWaterChar").charAt(0);
        sLandChar = prop.getProperty("sLandChar").charAt(0);
    }
}
