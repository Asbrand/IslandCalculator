/*
 * Copyright none
 */
package CountIslands.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Getting data through the Spring JDBC
 */
public class SpringJdbc extends DataConnector {

    JdbcTemplate jdbcTemplate;
    private final String SELECT_MAP = "SELECT * FROM public.\"IslandsMaps\" WHERE id = ?";

    @Autowired
    public void setJdbcTemplate(JdbcTemplate template) {
        jdbcTemplate = template;
    }

    @Override
    public String getMapRaw(long id) {
        MapEntity mapEntity = (MapEntity) jdbcTemplate.query(SELECT_MAP, new Object[]{id}, new BeanPropertyRowMapper(MapEntity.class)).get(0);
        return mapEntity.getMap();
    }

}
