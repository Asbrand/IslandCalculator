/*
 * Copyright none
 */
package CountIslands.DAO;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Класс описывающий объект возвращаемый из источника данных
 */
@Entity
@Table(name = "\"IslandsMaps\"", schema = "public")
public class MapEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "map")
    private String map;

    public MapEntity() {

    }

    public MapEntity(Long id, String map) {
        this.id = id;
        this.map = map;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

}
