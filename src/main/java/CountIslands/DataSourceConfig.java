/*
 * Author: kkolchanov 31.08.2018
 * Copyright none
 */
package CountIslands;

import CountIslands.DAO.Hibernate;
import CountIslands.DAO.IMapDao;
import CountIslands.DAO.SpringJdbc;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Файл конфигурации источников данных
 */
@Configuration
@ComponentScan
@PropertySource("classpath:/app.properties")
public class DataSourceConfig {

    @Autowired
    Environment env;

    @Bean
    @Description("Класс для подсчёта островов")
    IslandCalculator islandCalculator() {
        return new IslandCalculator();
    }

    @Bean
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource source = new DriverManagerDataSource();
        source.setDriverClassName(env.getProperty("db.driver"));
        source.setUrl(env.getProperty("db.url"));
        source.setUsername(env.getProperty("db.username"));
        source.setPassword(env.getProperty("db.password"));
        return source;
    }

    @Bean
    public JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public IMapDao getDAO() {
        return new Hibernate();
    }


    @Bean
    public SessionFactory getSessionFactory() {
        org.hibernate.cfg.Configuration configuration = new org.hibernate.cfg.Configuration();
        configuration.configure("hibernate.xml");

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        return sessionFactory;
    }

}
